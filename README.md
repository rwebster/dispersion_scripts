DigitalMicrograph (DM) scripts which perform the acquisition and processing for methods presented in the manuscript "Correction of EELS dispersion non-uniformities for improved chemical shift analysis"

The file "Dispersion_Test_Acquisition_v20190325.s" contains the script used to execute the automated acquisition.

The file "Dispersion_Corrector_v20200205.s" contains the code used for processing the data obtained by the acquisition script and applying corrections to real data.