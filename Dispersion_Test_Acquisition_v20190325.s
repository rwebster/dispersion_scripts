//////////////////////////////////////////////
// (c) 2018  B.Schaffer, Gatan				//
//////////////////////////////////////////////
// This script will create a "sweep" ZLP stack
// in a DEELS acquisition and follows procedures
// described by A.Craven, Glasgow University.
//////////////////////////////////////////////
// Version 19.03.2019:	Adds ability to read spectrometer settings once they are changed.
//
// Version 13.04.2018:	Corrected use of GetDate() to be consistent with the GMS2.3 format of
//						GetDate(number DateFormat, string DateString).  Dateformat has been declared
//						globally to ensure format is kept consistent.
//
// Version 05.04.2018:	Added time measurement of experiment.
//
// Version 26.03.2018:	The following changes have been made following testing:
//						-the function Main has changed name to CallZLPSweep to avoid potential 
//						 conflict with other scripts.
//						-When carrying out doShiftToRightCCDRange, the value eV to shift
//						 has been corrected to be calculated by multiplying by, instead of
//						 dividing by, the dispersion.
//						-The variable dispVal has been declared as a global variable so that
//						 it can also be used inside HandleDataValueChangedAction to determine
//						 when the end of the dispersion range has been reached to terminate
//						 the experiment.
//						-Result statements have been commented out in HandleDataValueChangedAction
//						 to reduce computation time during high-speed spectrometer reads.
//						-The variables maxX1, maxY1, maxX2, maxY2 in the CallZLPSweep function are
//						 now declared before the doPerformChannel200Setup conditional statement, to
//						 prevent the variables being declared twice.
//						-An OKDialog box is called after running doShiftToRightCCDRange.  This prevents
//						 an issue where the max functions on the following lines read the position of
//						 the ZLP before it is actually shifted.
//						-The value of variable DTMax has been adjusted to 2000 to conform to our specs.
//						-Additional Tags have been added to the images to keep track of the channel step
//						 and also the right hand buffer size.
//
// Version 12.01.2018:	First draft, coded and tested on GMS 3.3 with Faux System
//						Don't expect it to run out-of-the box, in particular for GMS 2.
//						No "cleanup" or finishing of tags etc. 
//////////////////////////////////////////////
//
// Instructions:
//		Set up DualEELS live view, i.e. have a running EELS spectroscopy view 
//		with low-loss and high-loss both set to exposures that the ZLP does not saturate.
//		( Don't use auto-exposure to avoid changes during series acquisition. )
//		Use the dispersion and readout-settings of your choice. The script will
//		just use whatever is in the running VIEW.
//
//		Set the drift-tube (shift between LL and HL ) to 0 V.
//		Set EnergyLoss (prism) to 0 V.
//		Align ZLP (with prism "align" or by automatic routine) to have ZLP at channel 200
//
//		With the view running, start the script.
//		Select Low-Loss and High-Loss View and specify settings, in particular
//			- The StepSize (in channels or eV) the ZLP should be moved in each step
//			- How many update-frames should be "skipped" after a drift-tube change.
//				This should account for settling time and will need to be empirical.
//
//		A dialog will indicate that the acquisition can start (OK button)
//		During acquisition, don't mess with the UI and leave it alone. 
//		If it needs to be interrupted, keep SHIFT and CTRL pressed until a dialog appears
//		and the results window prints that the script eneded (and the ListenerObject has been destroyed.)
//
//		"Hardcoded" parameters in the script are described below.
//////////////////////////////////////////////
//		NO WARRANTY OF ANY SORTS. USE AT YOUR OWN PERIL.
//////////////////////////////////////////////

number TRUE = 1
number FALSE = 0

number prismDelay_ms = 500
// ms waiting time after setting a prism value

number kFlipAdjust = 1	
// use -1 if Energy-Adjust uses flipped sign (GMS 2 vs GMS 3.3 ? Not sure, needs testing )
// For GMS 3.3 using a positive Adjust value shifts the ZLP to the right (higher loss).

number kRightChannelLimit = 20	
// Where to place the initial ZLP counted form right edge, value in channels.

number DTmax = 2000	
// Maximum value (eV) the drift-tube can be set. Is it 1000 V ?

number doPerformChannel200Setup = TRUE	
// If true, performs step where offset,align and drift-tube are set to 0. Then checks if
// ZLP maximum is (close to) channel 200 in both cases. If not: Align your system that it is 
// there for those values, i.e. use align and "nullify" it once the alignment is done.

number doShiftToRightCCDRange = TRUE
// If true, places the ZLP at wanted initial position on the right hand side of the CCD.
// If set to false (and the above also set to false) the script should be started after manually 
// setting the system to have the ZLP at wanted position.

number shiftMode = 2
// Method of shifting ZLP to RHS at start of acquistion.  
// current default is 4 for prism adjust.
//Set to 2 to use HT offset.
//may add script to minimise user manipulation of script.

number dispVal 
//declaring this as global so it can be read in the listener function

number StartTick, EndTick
number dateFormat = 1
// Measure experiment time

class CViewListener
{
	number elID1, srcImgID1, dstImgID1, dvID
	number srcImgID2, dstImgID2
	number nSkip, nHit, bStarted, nPos, nCh
	number keepShiftAdjust
	number dEV
	
	~CViewListener( object self ) { Result( "\n ListenerObject destructed." ); OKDialog( "Acquisition complete"); }
	object AddListener( object self, image src1, image dst1, image src2, image dst2, number deltaEV, number skipCounter, image Voltages )
	{
		if ( !src1.ImageIsValid() || !dst1.ImageIsValid() ) Throw( "Invalid input images" )
		if ( !src2.ImageIsValid() || !dst2.ImageIsValid() ) Throw( "Invalid input images" )
		if ( src1.ImageGetDimensionSize(0) != dst1.ImageGetDimensionSize(0) ) Throw( "Invalid input image sizes" )
		if ( src2.ImageGetDimensionSize(0) != dst2.ImageGetDimensionSize(0) ) Throw( "Invalid input image sizes" )
		if ( skipCounter < 0 ) Throw( "Invalid skip counter" )
	
		keepShiftAdjust = IFGetEnergyOffset( 4 )
		string messagemap = "data_value_changed" 	+ ":HandleDataValueChangedAction;"
		srcImgID1 = src1.ImageGetID()
		dstImgID1 = dst1.ImageGetID()
		srcImgID2 = src2.ImageGetID()
		dstImgID2 = dst2.ImageGetID()
		dvID = Voltages.ImageGetID()
		nSkip = skipCounter
		nHit = 0
		bStarted = 0
		nPos = 0
		nCh = src1.ImageGetDimensionSize(0)
		dEV = deltaEV
		Debug("\n delta eV:"+deltaEV)
		Debug("\n nSkip:"+nSkip)
		elID1 = src1.ImageAddEventListener( self, messagemap )
		
		return self
	}
	
	void EnableListening( object self, number doListen )
	{
		bStarted = doListen	
	}
	
	void RemoveListener( object self )
	{
		image src1 := GetImageFromID( srcImgID1  )
		if ( src1.ImageIsValid() )
			src1.ImageRemoveEventListener( elID1 )
		elID1  = 0	
	}
	
	void Finish( object self )
	{
		if ( !bStarted ) return
		bStarted = 0
		EndTick = GetHighResTickCount()
		number elapsedTime = ( EndTick - StartTick ) / GetHighResTicksPerSecond()
		Result("\n Finishing ( resetting drift tube and shift ) - duration: " + elapsedTime + " sec" )
		self.RemoveListener()
		
		image dst1 := GetImageFromID( dstImgID1 )
		
		string dateString
		GetDate(DateFormat,DateString)
		
		if ( dst1.ImageIsValid() )
		{
			dst1.SetNumberNote( "Test Notes:Elapsed time (sec)",elapsedTime)
			dst1.SetStringNote( "Test Notes:Experiment end" , DateString + " @ " + GetTime(1) )
			dst1.SetNumberNote( "Test Notes:Prism Adjust", IFGetEnergyOffset(4))
			dst1.SetNumberNote( "Test Notes:HT Offset", IFGetEnergyOffset(2))
			dst1.ShowImage()
		}
		image dst2 := GetImageFromID( dstImgID2 )
		if ( dst2.ImageIsValid() )
		{
			dst2.SetNumberNote( "Test Notes:Elapsed time (sec)",elapsedTime)
			dst2.SetStringNote( "Test Notes:Experiment end" , DateString + " @ " + GetTime(1) )
			dst2.SetNumberNote( "Test Notes:Prism Adjust", IFGetEnergyOffset(4))
			dst2.SetNumberNote( "Test Notes:HT Offset", IFGetEnergyOffset(2))
			dst2.ShowImage()
		}

		IFSetEnergyOffset( 3, 0 )
		IFSetEnergyOffset( 4, 0 )
		sleep( prismDelay_ms/1000 )
	}
	
	Void HandleDataValueChangedAction(object self, number e_fl, Image img )
	{
		if ( !bStarted ) return
		if ( OptionDown() && ShiftDown() )
		{
			Result("\n Aborting")
			self.Finish()
		}
				
		nHit++
		if ( nHit <= nSkip ) return
		
		nHit = 0
		image dstLL,dstHL,srcLL,srcHL, driftVoltage
		Try{
			dstLL := GetImageFromID( dstImgID1 )
			dstHL := GetImageFromID( dstImgID2 )
			srcLL = GetImageFromID( srcImgID1 )	// value copy
			srcHL = GetImageFromID( srcImgID2 )	// value copy
			driftVoltage := GetImageFromID(dvID)
		}
		catch
		{
			self.Finish()
			return
			break;
		}
			
		if ( dstLL.ImageGetDimensionSize(1) <= nPos )
		{
			self.Finish()
			return
		}
		
		dstLL.slice1( 0,nPos,0, 0,nCh,1 ) = srcLL
		dstHL.slice1( 0,nPos,0, 0,nCh,1 ) = srcHL
		
		
		
		// Set next shift value
		if ( 0 != dEV )
		{
			number isValue = IFGetEnergyOffset( 3 )
			dstLL.SetNumberNote( "Test Notes:VSM:"+nPos , isValue )
			dstHL.SetNumberNote( "Test Notes:VSM:"+nPos , isValue )

			if ( isValue + dEV <= ((DTmax - kRightChannelLimit) * dispVal )) //adjusts DTMax by dispersion to stop shift dropping of LHS of detector
			{
				//Result( "\n\n Step #" + nPos + " changing " + dEv + " eV: " + (isValue+deV) + " eV" )
				IFSetEnergyOffset( 3, isValue + dEV )	// Using driftTube
			}
			else
			{
				Result("\n Warning: Exceeding limits. Will not set new drift tube value." )
				self.Finish()
				return
			}

		}
		nPos++
	}
}

void CallZLPSweep()
{
	// Performing intial checks, asking the user to have
	// everything set up in advance.

	if ( !IFVerifyCommunication() )
		Throw( "No Filter System found." )

	if ( !IFIsInSpectroscopyMode() )
		Throw( "System needs to be in EELS mode" )

	image LLview, HLview
	if ( !GetTwoLabeledImagesWithPrompt( "Please select the two DEELS view images of a currently running live View","Select View images", "low loss",LLview,"high loss",HLview) )
		exit(0)

	// Verify the two images are DEELS silblings
	tagGroup LLtg = LLview.ImageGetTagGroup()
	tagGroup HLtg = HLview.ImageGetTagGroup()

	number doDEELS = 0
	LLtg.TagGroupGetTagAsBoolean("EELS:Acquisition:Dual acquire enabled", doDEELS )
	if ( !doDEELS )
		Throw( "Image '"+LLview.GetLabel()+"' is not a Dual-EELS acquistion image" )
	HLtg.TagGroupGetTagAsBoolean("EELS:Acquisition:Dual acquire enabled", doDEELS )
	if ( !doDEELS )
		Throw( "Image '"+HLview.GetLabel()+"' is not a Dual-EELS acquistion image" )

	number isLL = 0
	LLtg.TagGroupGetTagAsBoolean("EELS:Acquisition:Is dual acquire low-loss", isLL )
	if ( !isLL )
		Throw( "Image '"+LLview.GetLabel()+"' is not a Dual-EELS acquistion low-loss image" )

	HLtg.TagGroupGetTagAsBoolean("EELS:Acquisition:Is dual acquire low-loss", isLL )
	if ( isLL )
		Throw( "Image '"+HLview.GetLabel()+"' is not a Dual-EELS acquistion high-loss image" )

	if ( HLView.ImageGetDimensionSize(0) != LLView.ImageGetDimensionSize(0) )
		Throw( "Size mismatch of the two view images." )

	if ( 1 != LLView.ImageGetDimensionSize(1) )
		Throw( "EELS View images needs to be a 1D image" )

	number nChannels = LLView.ImageGetDimensionSize(0)
	number nomDispVal = LLView.ImageGetDimensionScale(0)
	number LLexp, HLexp
	if ( !LLTg.TagGroupGetTagAsNumber( "EELS:Acquisition:Exposure (s)", LLexp ) )
		Throw( "Image '"+LLview.GetLabel()+"' exposure tag not found" )

	if ( !HLTg.TagGroupGetTagAsNumber( "EELS:Acquisition:Exposure (s)", HLexp ) )
		Throw( "Image '"+HLview.GetLabel()+"' exposure tag not found" )

		
	// Reading current setting
	number apNum = IFGetActiveAperture( )
	string apStr = IFGetApertureLabel( apNum ) 

	number dispInd = IFGetActiveDispersionIndex( )
	string dispStr = IFGetActiveDispersionLabel( )
	dispVal = IFGetActiveDispersion( ) 		// [eV/ch] //this is now declared as a global variable in line 65

	
	if ( dispVal != nomDispVal )
		OKDialog( "WARNING: The dispersion in the image does not seem to match the nominal dispersion." )
	number maxX1,maxY1, maxX2,maxY2
	if ( doPerformChannel200Setup )
	{
		if ( !OKCancelDialog( "Ensure DEELS view is running. Setting ZLP to channel #200 for both views." ) )
			exit(0)

		IFSetEnergyLoss( 0 )		// Whatever is "linked" ( Prism for EELS)
		IFSetEnergyOffset( 1, 0 )	// Prisim offset ("SHIFT")
		IFSetEnergyOffset( 2, 0 ) 	// HT offset
		IFSetEnergyOffset( 4, 0 )	// Prisim offset ("Align")
		IFSetEnergyOffset( 3, 0 )	// Drift Tube 
		sleep( prismDelay_ms/1000 )
		
		//number maxX1,maxY1, maxX2,maxY2
		max( LLView, maxX1, maxY1 )
		max( HLView, maxX2, maxY2 )
			
		Result("\n ZLP positons at nominal 0 eV:")
		Result("\n   low - loss : channel #" + maxX1 )
		Result("\n   high - loss: channel #" + maxX2 )
		
		if ( ( maxX1 < 190 ) || ( maxX1 > 210 ) )
			Throw( "The low-loss ZLP is not at channel 200. (" + maxX1+ ")" )
		if ( ( maxX2 < 190 ) || ( maxX2 > 210 ) )
			Throw( "The high-loss ZLP is not at channel 200. (" + maxX2+ ")" )
	}

	if ( 0 != IFGetEnergyOffset( 3 ) )	// Drift Tube  )
	{
		OKDialog( "Adjusting low-loss / high-loss relative shift to 0 eV" )
		IFSetEnergyOffset( 3, 0 )	// Drift Tube 
	}

	number store_prismAdjust = IFGetEnergyOffset( 4 )
	number store_HToffset = IFGetEnergyOffset( 2 )
	if ( doShiftToRightCCDRange )
	{
		number eVtoShift = trunc(nChannels-200-kRightChannelLimit)*dispVal  //divide changed to multple by
		if ( eVtoShift > DTmax  )
		{
			OKDialog( "Using the maximum drift tube range ("+DTmax +" eV) for shifting, but will not reach desired channel." )
			eVToShift = DTmax 
		}
		//Shifting ZLP with prism shift (using adjust)
		if( shiftMode==4 )
		{
			IFSetEnergyOffset( 4, store_prismAdjust + eVtoShift * kFlipAdjust )	// Prisim offset ("SHIFT")
		}
		else if( shiftMode==2 )
		{
			IFSetEnergyOffset( 2, store_HToffset - eVtoShift * kFlipAdjust ) //HT offset
		}
		else
		{
			OKDialog( "Improper shift mode selected.  Aborting..." )
			exit(0)
		}
		IFSetEnergyOffset( 3, 0 )	// Drift Tube 
	}
	OKDialog("Ready?")
	//number maxX1,maxY1, maxX2,maxY2
	max( LLView, maxX1, maxY1 )
	max( HLView, maxX2, maxY2 )
	Result("\n ZLP positons at experiment start:")
	Result("\n   low - loss : channel #" + maxX1 )
	Result("\n   high - loss: channel #" + maxX2 )

	number ZLPshiftStep_Ch = 5
	number ZLPshiftStep_eV

	if ( !GetNumber( "Wanted ZLP shift step (channels) ?", ZLPshiftStep_Ch, ZLPshiftStep_Ch ) ) 
		exit(0)
		
	ZLPshiftStep_eV = trunc(ZLPshiftStep_Ch) * dispVal
	if ( !GetNumber( "Wanted ZLP shift step (eV) (now "+trunc(ZLPshiftStep_Ch)+" channels) ?", ZLPshiftStep_eV, ZLPshiftStep_eV ) ) 
		exit(0)
	
	ZLPshiftStep_Ch = trunc( ZLPshiftStep_eV / dispVal )
	ZLPshiftStep_eV = trunc( ZLPshiftStep_eV * 10 ) / 10 //round to the nearest 0.1eV
			
	number nSkipFrames = 0
	string message = "Current exposures are ( " + LLexp + " / " + HLexp + " )\n"
	message += "\n\n Skip how many frames between capture?"
	message += "\n (Maybe settling time for drift tube...)"
	If ( !GetNumber( message, LLexp<0.01 ? 5 : 1, nSkipFrames ) )
		exit(0)
		
	// Setting up data containers
	number nSteps = trunc( maxX2 / ZLPshiftStep_Ch ) + 10
	result( "\n Moving the ZLP form channel #" + maxX2 + " left until edge is reached.")
	result( "\n Using step size of " + ZLPshiftStep_Ch + " channels ( " + ZLPshiftStep_eV + " eV)." )
	result( "\n Using " + nSteps + " steps maximum." )
		
	
	string dateString
	GetDate(DateFormat,DateString)

	image LLstack := LLView.ImageClone() 
	image HLstack := HLView.ImageClone()
	image driftVolts := LLView.ImageClone()
	LLStack.ImageResize(2,nChannels,nSteps)
	LLStack.ImageSetDimensionCalibration(1,0,1,"#",0)
	LLStack.SetName( "Low-loss series" )
	LLStack.SetStringNote( "Meta Data:Format","Spectrum image" )
	LLStack.SetStringNote( "Meta Data:Signal","EELS" )
	LLStack.SetNumberNote( "Test Notes:RHS Buffer" , kRightChannelLimit )
	LLStack.SetNumberNote( "Test Notes:ChStep" , ZLPshiftStep_Ch )
	LLStack.SetNumberNote( "Test Notes:DTmax" , DTmax )
	LLStack.SetNumberNote( "Test Notes:Ch200Setup" , doPerformChannel200Setup )
	LLStack.SetNumberNote( "Test Notes:ShiftToRight" , doShiftToRightCCDRange )
	LLStack.SetNumberNote( "Test Notes:dispval" , dispVal )
	LLStack.SetNumberNote( "Test Notes:Skip Frames" , nSkipFrames )
	LLStack.SetStringNote( "Test Notes:Experiment start" , DateString + " @ " + GetTime(1) )
	
	HLStack.ImageResize(2,nChannels,nSteps)
	HLStack.ImageSetDimensionCalibration(1,0,1,"#",0)
	HLStack.SetName( "High-loss series" )
	HLStack.SetStringNote( "Meta Data:Format","Spectrum image" )
	HLStack.SetStringNote( "Meta Data:Signal","EELS" )
	HLStack.SetNumberNote( "Test Notes:RHS Buffer",kRightChannelLimit)
	HLStack.SetNumberNote( "Test Notes:ChStep" , ZLPshiftStep_Ch )
	HLStack.SetNumberNote( "Test Notes:DTmax" , DTmax )
	HLStack.SetNumberNote( "Test Notes:Ch200Setup" , doPerformChannel200Setup )
	HLStack.SetNumberNote( "Test Notes:ShiftToRight" , doShiftToRightCCDRange )
	HLStack.SetNumberNote( "Test Notes:dispval" , dispVal )
	HLStack.SetNumberNote( "Test Notes:Skip Frames" , nSkipFrames )
	LLStack.SetStringNote( "Test Notes:Experiment start" , DateString + GetTime(1) )
	
	LLStack.DisplayAt(300,300)
	LLStack.ImageGetImageDisplay(0).ImageDisplaySetCaptionOn(1)
	
	HLStack.DisplayAt(300,600)
	HLStack.ImageGetImageDisplay(0).ImageDisplaySetCaptionOn(1)
	
	
	// Attaching display listeners to view
	object VLobj = Alloc(CViewListener).AddListener( LLView, LLStack, HLView, HLStack, ZLPshiftStep_eV, nSkipFrames, driftVolts )
	OKDialog( "Start now\n\n Keep SHIFT and CTRL pressed to abort." )
	StartTick = GetHighResTickCount()
	VLobj.EnableListening( 1 )
}

CallZLPSweep()