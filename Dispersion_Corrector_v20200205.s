///////////////////////////////////////////////////////////////////////////////
//		Dispersion Non-Uniformity Processing Script v20200205
//
//		Robert Webster, University of Glasgow, 2020
///////////////////////////////////////////////////////////////////////////////

/*

	This script is provided to process measurements taken using the 
	accompanying acquisition script.  The processing done in this script is 
	kept separate to the acquisition script so that it may be run offline away
	from the microscope.

	Certain elements of this script are adapted from mathematics or code 
	available in the literature.  For more information, please see the 
	following references:

	References:
	===========

	-Interpolation Routine:
		"Numerical Recipes; the Art of Scientific Computing", Third Edition,
		Chapter 3, pp114-121
	-Linear regression function:
		"Numerical Recipes; the Art of Scientific Computing", Third Edition,
		Chapter 15, pp780-785
	-Transformation intensity correction:
		"Get the Basics Right: Jacobian Conversion of Wavelength and Energy
		Scales for Quantitative Analysis of Emission Spectra" J.Phys.Chem.Lett,
		2013, 4, pp3316-3318, dx.doi.org/10.1021/jz401508t

	===========

	Note that in the lengthy parts of the processing, the progress box should
	update regularly, however DM may freeze up and become unresponsive.  If 
	this happens, allow DM a few minutes to complete the processing, as the
	background threads which manage all the data handling should still be 
	working away.
	
	If you have any questions about using the script, please get in touch.
	
	Happy EELS-ing,

	Robert Webster

*/

///////////////////////////////////////////////////////////////////////////////
//		USER PARAMETERS
///////////////////////////////////////////////////////////////////////////////

Number bD = 8 // Sets the byte depth of image objects (4 or 8)
Number thresh = 0.36 // Fractional threshold for ZLP fit (default=0.36, which
// excludes all points with intensity below 0.36Imax)
Number zoomCh = 200 // The zoom point of the spectrometer given as channel num
// default zoom channel for Gatan Quantum 965 is 200
Number polyOrd = 5 // Order of polynomial to be used in a fit. (default=5)
Number usePolyFit = 1 // Set whether final correction is based results of
// polynomial fit.  1 for true, 0 for false. Only relevant for doAll. 
// Note: Do not change usePolyFit from 1.
Number useNomDisp = 1 // Use Nominal Dispersion.  If true, dispersion profile
// includes transform to nominal dispersion. If false, corrected data will have
// measured rather than nominal dispersion. (default=1)
Number zoomRef = 1 // Boolean, choose whether to use zoom channel (defined above)
// As the reference point in the non-uniformity profile
Number ZCor = 0 // Boolean, choose whether to include the Zoom Point Energy (ZPE)
// term in the correction which is applied to the data.  NOTE: if you are
// processing an SI where the high-loss offset was varied throughout, do not use 
// ZCor beforeensuring that the variation is removed and all HL data have the 
// same offset.

///////////////////////////////////////////////////////////////////////////////
//		GLOBAL PARAMETERS
///////////////////////////////////////////////////////////////////////////////

Number m0 = 9.10938356E-31 //electron rest mass (kg)
Number cSqr = 299792458**2 //speed of light squared (m^2/s^2)
Number eCh = 1.6021766208E-19 //charge of electron (C)
Number interp_param = 5 // Default is 5, for quartic interpolation

///////////////////////////////////////////////////////////////////////////////
//		INTERPOLATION CLASS
///////////////////////////////////////////////////////////////////////////////

class Poly_interp {
	/*
		This class provides interpolation based on non-uniformly distributed
		data.  It is adapted from the routines "Base_interp", "Linear_interp",
		and "Poly_interp" described in

		"Numerical Recipes; the Art of Scientific Computing", Third Edition,
		Chapter 3, pp114-121

		and should be referred to for further details.

		Usage Example:
		==============

		Object interpolator = Alloc(Poly_interp) // create object instance

		interpolator.init(xvalues, yvalues, m) // initialise with images which \
		// contain x points and y points and m, which is the polynomial order + 1
		//i.e. m=4 for cubic polynomial

		number val = interpolator.interp(i) // return interpolated value 'val' \
		// at position 'i'

	*/

	number n, mm, jsav, cor, dj, sy, sz
	image xx, yy, dy
	
	void init(object self, image x, image y, number m) {
		n = x.ImageGetDimensionSize(0)
		sy = y.ImageGetDimensionSize(1)
		if(y.ImageGetNumDimensions()==3) {
			sz = y.ImageGetDimensionSize(2)
		}
		else { sz = 1; }
		mm = m
		jsav = 0
		cor = 0
		xx = x
		yy = y
		dj = min(1, round(n**(0.25)))
	}
	
	number locate(object self, number x) {
		
		number ju, jm, jl
		if(n < 2||mm < 2||mm > n) throw("locate size error")
		number ascnd = xx.GetPixel(n-1,0) >= xx.GetPixel(0,0)
		jl = 0
		ju = n - 1
		while(ju - jl > 1) {
			jm = (ju + jl) >> 1
			if(x >= xx.GetPixel(jm,0) == ascnd) jl = jm
			else ju = jm
		}
		cor = abs(jl - jsav) > dj ? 0 : 1
		jsav = jl
		return max(0, min(n - mm, jl - ((mm-2)>>1)))
	}
	
	number hunt(object self, number x) {
		number jl = jsav
		number jm, ju
		number inc = 1
		if(n < 2 || mm < 2 || mm > n) throw("hunt size error")
		number ascnd = xx.GetPixel(n-1,0) >= xx.GetPixel(0,0)
		if(jl < 0 || jl > n-1) {
			jl = 0
			ju = n-1
		}
		else {
			if(x >= xx.GetPixel(jl,0) == ascnd) {
				while(ImageIsValid(xx)) {
					ju = jl + inc
					if(ju >= n-1) { ju = n-1; break; }
					else if(x < xx.GetPixel(ju,0) == ascnd) break; 
					else { jl = ju; inc += inc; }
				}
			}
			else {
				ju = jl
				while(ImageIsValid(xx)) {
					jl = jl - inc
					if(jl <= 0) { jl = 0; break; }
					else if(x >= xx.GetPixel(jl,0) == ascnd) break;
					else { ju = jl; inc += inc; }
				}
			}
		}
		while(ju - jl > 1) {
			jm = (ju+jl) >> 1
			if(x >= xx.GetPixel(jm,0) == ascnd) jl = jm
			else ju = jm
		}
		cor = abs(jl - jsav) > dj ? 0 : 1
		jsav = jl
		return max(0, min(n-mm, jl-((mm-2)>>1)))
	}
	
	image rawinterp(object self, number jl, number x) {
		number i = 0, m = 0, ns = 0
		number dif, dift, ho, hp
		image y, w, den
		image xa = xx.slice1(jl, 0, 0, 0, mm, 1)
		image ya = yy.slice3(jl, 0, 0, 0, mm, 1, 1, sy, 1, 2, sz, 1)
		image c = ya.imageClone(), d = ya.ImageClone()
		dif = abs(x - xa.GetPixel(0, 0))
		
		for(i=0; i<mm; i++) {
			dift = abs(x-xa.GetPixel(i, 0))
			if(dift < dif) {
				ns = i
				dif = dift
			}
		}

		y = ya.slice2(ns--, 0, 0, 1, sy, 1, 2, sz, 1)
		
		for(m=1; m<mm; m++) {
			for(i=0; i<(mm-m); i++) {
				ho = xa.GetPixel(i, 0) - x
				hp = xa.GetPixel(i+m, 0) - x
				w = c.slice2(i+1, 0, 0, 1, sy, 1, 2, sz, 1) - d.slice2(i, 0, 0, 1, sy, 1, 2, sz, 1)
				if((ho-hp) == 0.0) { throw("Poly_interp error."); }
				den = w/(ho-hp)
				d.slice2(i, 0, 0, 1, sy, 1, 2, sz, 1) = hp*den
				c.slice2(i, 0, 0, 1, sy, 1, 2, sz, 1) = ho*den
			}
			if(2*(ns+1) < (mm - m)) { dy = c.slice2(ns+1, 0, 0, 1, sy, 1, 2, sz, 1); }
			else { dy = d.slice2(ns--, 0, 0, 1, sy, 1, 2, sz, 1); }
			y += dy
		}
		return y
	}
	
	image interp(object self, number x) {
		number jlo = cor ? self.hunt(x) : self.locate(x)
		return self.rawinterp(jlo, x)
	}	
}

///////////////////////////////////////////////////////////////////////////////
//		NON-UNIFORMITY PROCESSING CLASS
///////////////////////////////////////////////////////////////////////////////

class correctionProcs {
	/*
		Class which actually performs the processing and data correction.
	*/

	// Create References to class attributes:
	image ref, disp, driftTube, LL, HL // references to raw data
	//references to acquisition tags
	number shiftStepCh, limRHS, epch, prism, HT, HToffset
	//references to results
	image deviations, refMap, dispMap, refMax, dispMax, rFit, dFit, \
		interpol, intCorrMapL, intCorrMapH, LL_CECI, HL_CECI

	number nDat, nCh // references to data sizes

	number retNumTag(object self, taggroup tg, string tagPath, number dflt) {
		/*
			retNumTag (retrieve Number Tag) is a general wrapper function
			which returns a specific piece of metadata.

			Parameters:
			===========
			tg:			taggroup
						The taggroup which contains the desired information.
			tagPath:	string
						The path to teh tag with the relevant info.
			dflt:		number
						the default value to be used if no new info is provided.
			Returns:
			========
			value:		number
						The value which is either retrieved from tag or
						provided by the user
		*/
		number value
		// First check if information is in tag structure
		if(!tg.TagGroupGetTagAsNumber(tagPath, value)) { 	
			// If not in tags, ask user to provide value manually
			if(!GetNumber("Tag not found.  Please provide the number that " \
				+"should have been provided by "+tagPath, dflt, value)) {
					throw("Metadata "+tagPath+" not provided. Exiting...")
					exit(0)
			}
		}
		return value
	}

	void init(object self, number doAll) {
		/*
			The init function searches the tags and locates all the relevant
			metadata for the processing.  For some metadata, the user is asked
			for the information if the relevant tag is not found.

			Paramters:
			==========
			doAll:	number
					Either 1 or 0.  Tells the class whether all methods in class
					will be executed.  If 1, init() knows to ask for references
					to additional images.
		*/
		image temp1, temp2, temp3, temp4
		// First, retrieve the images which contain the alignment measurment
		// If these are not provided, the script exits.
		if( !GetTwoLabeledImagesWithPrompt("Choose ZLP Sweep Data","Load" \
			+" reference ZLP and scanned ZLP images.", \
			"Reference", temp1, "Scanned", temp2) ) exit(0)
		// Make copies of the data in the class attributes
		ref := temp1.ImageClone()
		disp := temp2.ImageClone()
		nCh = ref.ImageGetDimensionSize(0)
		// Extract tag structure from one of the images
		taggroup acquiTags = ImageGetTagGroup(ref)
		// Start extracting information from the tags
		shiftStepCh = self.retNumTag(acquiTags, "Test Notes:ChStep", 10)
		limRHS = self.retNumTag(acquiTags, "Test Notes:RHS Buffer", 20)
		epch = self.retNumTag(acquiTags, "Test Notes:dispval", 0.5)
		prism = self.retNumTag(acquiTags, "Test Notes:Prism Adjust", 0)
		HToffset = self.retNumTag(acquiTags, "Test Notes:HT Offset", 0)
		HT = self.retNumTag(acquiTags, "Microscope Info:Voltage", 200000)
		// Retrieve VSM values
		taggroup VSMtags
		if(TagGroupGetTagAsTagGroup(acquiTags, "Test Notes:VSM", VSMtags)) {
			nDat = TagGroupCountTags(VSMtags)
			driftTube := RealImage("VSM Scan Voltages", bD, nDat, 1)
			for(number i=0; i<nDat; i++) {
				number val
				TagGroupGetTagAsNumber(acquiTags, "Test Notes:VSM:"+i, val)
				driftTube[i,0] = val
			}
		} 
		else {
			if(OKCancelDialog("VSM Values not found, create mock values?")) {
				GetNumber("Number of acquisitions in ZLP sweep?", 100, nDat)
				driftTube := RealImage("VSM Scan Voltages", bD, nDat, 1)
				driftTube = (icol)*epch*shiftStepCh
			}

		}
		// Get data to be corrected if performing all steps
		if(doAll) {
			GetTwoLabeledImagesWithPrompt("Choose real DualEELS data to be corrected.", \
				"Choose real DualEELS data to be corrected.", "Low-Loss", \
				temp3, "High-Loss", temp4)
			LL := temp3.ImageClone()
			HL := temp4.ImageClone()
		}
	}

	taggroup windowByInt(object self, image data, number centre, number intCut) {
		/*
			Takes a single acquisition frame and finds the bounds at which the
			ZLP drops below a certain intensity.

			Parameters:
			--------------------
			data:	image
					A 1d spectrum with a ZLP to be windowed

			centre:	number
					The pixel value of the midpoint of the ZLP (probably
					found by the first pass of mapZLP())

			intCut:	number
					The intensity cutoff threshold (expressed as a fraction)
					at which to define the indices marking the window bounds.

			Returns:
			--------------------
			bounds:	taggroup
					TagList of length 2 containing first the left bound and
					the right bound second
		*/
		number dMin, dMax, left, right, xlim
		minmax(data, dMin, dMax) // find minimum and maximum values in frame
		number cutOff = intCut*(dMax - dMin) + dMin //determine cutoff value
		//Search for left bound
		for(number j=centre-1; j>0; j--) {
			left = j
			number isVal = data.GetPixel(j,0)
			if(isVal<cutOff) { break; }
		}
		//Search for right bound
		xlim = ImageGetDimensionSize(data, 0)
		for(number j=centre+1; j<xlim; j++) {
			right = j
			number isVal = data.GetPixel(j,0)
			if(isVal<cutOff) { break; }
		}
		taggroup bounds = NewTagList()
		TagGroupInsertTagAsNumber(bounds, 0, left)
		TagGroupInsertTagAsNumber(bounds, 1, right)
		return bounds
	}

	image doGaussFit(object self, image data, taggroup threshold) {
		/*
			Wrapper for usage of FitGaussian() builtin function.
			Applies thresholding determined by windowByInt() before performing
			Gaussian fit.

			Parameters:
			===========
			data:		image
						A 1d image containing the data to be cropped and fitted.
			threshold:	taggroup
						A taggroup containing the pixel coordinates which will
						be used to threshold the data.
			Returns:
			========
			results:	image
						A 1x4 image containing the fit paramters and chiSquared
						values in each pixel.
		*/
		number left, right
		threshold.TagGroupGetIndexedTagAsNumber(1, right)
		threshold.TagGroupGetIndexedTagAsNumber(0, left)
		// Crop data based on threshold for fit
		image crop = data.slice1(left, 0, 0, 0, (right - left), 1)
		// prepare fit
		number amp, centre, width, conv, chisq, _
		conv = 0.00001; chisq = 1e6;
		image errors := crop.ImageClone()*0 + 1
		// provide initial guesses for paramters
		amp = max(crop, centre, _)
		width = 1/epch
		// Perform fit
		number OK = FitGaussian(crop, errors, amp, centre, width, chisq, conv)
		// prepare results to be returned
		image results := RealImage("Fit Results", bD, 4, 1)
		results[0, 0] = amp
		results[1, 0] = centre + left
		results[2, 0] = width
		results[3, 0] = chisq

		return results
	}

	void fitAB(object self, image xdata, image ydata, number &a, number &b) {
		/*
			Function which performs a linear regression on a nonuniformly 
			spread dataset, requires X and Y values as separate 1d inputs.
			Adapted from "Numerical Recipes" Chapter 15.

			Parameters:
			===========
			xdata:	image
					1d image containing x-values of data to be fitted
			ydata:	image
					1d image containing list of y-values to be fitted
			a:		reference to number
					The y intercept to be found
			b: 		reference to number
					The analysed linear gradient

			Returns:
			========
			results:	taggroup
						list containing the y intercept, gradient and goodness
						of fit
		*/
		number i, ss = 0, sx = 0, sy = 0, st2 = 0, sxoss, siga, sigb, \
			chi2, sigdat, ndata, hold

		image t

		xdata.GetSize(ndata,hold)
		
		sx = sum(xdata)
		sy = sum(ydata)

		ss = ndata
		sxoss = sx/ss

		t = xdata - sxoss
		st2 = sum(t**2)
		b = sum(t*ydata)
		
		b /= st2
		a = (sy-sx*b)/ss
		siga = sqrt((1+sx*sx/(ss*st2))/ss)
		sigb = sqrt(1/st2)
		for(i=0; i<ndata; i++) chi2 += (GetPixel(ydata,i,0)-a-b*GetPixel(xdata,i,0))**2

		chi2 = sum((ydata - a - b*xdata)**2)
		
		if(ndata > 2) sigdat = sqrt(chi2/(ndata-2))
		
		siga *= sigdat
		sigb *= sigdat
	}

	void fitZLP(object self) {
		/*
			Maps the centre position of the zero-loss peak across the detector
			with single-pixel accuracy as a quick first pass calculation

			nDat is now determined from counting VSM tags in init().
		*/
		refMap := RealImage("Single-pixel precision reference positions", bD, nDat, 1)
		dispMap := RealImage("Single-pixel precision scanned positions", bD, nDat, 1)
		refMax := RealImage("Reference peak intensities", bD, nDat, 1)
		dispMax := RealImage("Scanned peak intensities", bD, nDat, 1)

		for(number i=0; i<nDat; i++) {
			number refPos, dispPos, _
			refMax[i,0] = max(ref.slice1(0,i,0,0,nCh,1), refPos, _)
			dispMax[i,0] = max(disp.slice1(0,i,0,0,nCh,1), dispPos, _)
			refMap.SetPixel(i,0,refPos)
			dispMap.SetPixel(i,0,dispPos)
		}
	}

	void refineFitSubPix(object self) {
		/*
			Provides sub-pixel refinement to the centre position peak
			measurements provided by fitZLP().
		*/
		number tick, tock // variables for duration tracking
		tick = GetHighResTickCount() // Mark start time

		rFit := RealImage("Reference Peak Fit Coefficiencts", bD, nDat, 4)
		dFit := RealImage("Scanned Peak Fit Coefficiencts", bD, nDat, 4)

		for(number i=0; i<nDat; i++) {
		// loop over each frame in acquisition
			taggroup rBounds, dBounds
			image refslice = ref.slice1(0, i, 0, 0, nCh, 1)
			image dispslice = disp.slice1(0, i, 0, 0, nCh, 1)
			// Determine window by intensity thresholding
			rBounds = self.windowByInt(refslice, sum(refMap[i,0]), thresh)
			dBounds = self.windowByInt(dispslice, sum(dispMap[i,0]), thresh)

			image rOut, dOut // containers for fit results
			rOut = self.doGaussFit(refslice, rBounds)
			dOut = self.doGaussFit(dispslice, dBounds)

			// Append fit coefficients for this frame to results
			rFit.slice1(i, 0, 0, 1, 4, 1) = rOut
			dFit.slice1(i, 0, 0, 1, 4, 1) = dOut
			// Update progress palette
			number prog = round(i/nDat*100)
			if(remainder(i,floor(nDat/20))==0) 
				{ OpenAndSetProgressWindow("ZLP fitting in progress.","",prog+"% Complete"); }
		}
		// Update progress palette with total time elapsed
		tock = GetHighResTickCount() // Mark calculation end time
		number elapsedTime = (tock - tick) / GetHighResTicksPerSecond()
		OpenAndSetProgressWindow("ZLP map completed.","","Duration: "+elapsedTime+" sec")
	}

	image generateProfile(object self) {
		/*
			Compares measured peak positions on detector with recorded VSM 
			values to calculate the position-specific dispersion and return
			a nonuniformity profile which can be used to transform data 
			onto the nominal energy dispersion.

			Returns:
			========
			profile:	image
						an nCh pixel long 1d image containing the nonuniformity
						calculations across the detector range
		*/
		image rPos, dPos, Pos
		rPos = rFit.slice1(0, 1, 0 ,0, nDat, 1)
		dPos = dFit.slice1(0, 1, 0 ,0, nDat, 1)
		// Calculate the compensated position of the scanned peak on the detector
		//Pos = mean(rPos[0,0,1,nDat]) + dPos - rPos
		Pos = rPos.GetPixel(0,0) - (rPos - dPos)
		// Fit the slope of driftTube vs Pos to get measured value of dispersion
		// A customised regression function "fitAB" is used to handle the
		// non-uniform distribution of data-points
		number mDisp, ycept
		self.fitAB(Pos, driftTube, ycept, mDisp)
		result("Nominal dispersion is " + epch + " eV/ch. Calculated " \
			+"dispersion is " + -mDisp + " eV/ch. \n")

		if(!useNomDisp) { epch = abs(mDisp); } 
		// Perform interpolation over detector range to calculate profile
		number left = min(Pos)
		number right = max(Pos)
		number nChI = floor(right - left)
		// Create image which will hold interpolation values over data range
		// (no extrapolation here!)
		image profile := RealImage("Non-Uniformity Profile", bD, nDat, 1)
		object interpolator = Alloc(Poly_interp)
		interpolator.init(Pos, driftTube, interp_param)
		for(number i=0; i<nDat; i++) { 
			profile[i,0] = interpolator.interp(right - (nDat - i)*shiftStepCh); 
		}
		// Subtract dispersion from interpolation results to get nonuniformities
		profile -= (nDat - icol) * (epch * shiftStepCh)
		profile.ImageSetDimensionOrigin(0, right - (nDat-1)*shiftStepCh)
		profile.ImageSetDimensionScale(0, shiftStepCh)
		profile.SetNumberNote("Processing:Measured Dispersion", -mDisp)
		return profile
	}

	void calculateZPEnergyTerm(object self, image profile) {
		/*
			This function compares the observed position of the reference
			peak with its expected energy.  This is used to calculate the 
			gradient of a linear, energy-dependent term (deriving from the
			error in the Zoom Point Energy (ZPE)) in the correction which 
			is incorporated into the profile at the point of correcting
			real data.

			Parameters
			----------
			profile: 	Image
						A reference to the previously calculated non-uniformity
						profile which contains the position-dependent variations
						The calculated coefficient for the energy term will be 
						written into the tags of this image.
		*/
		image rPos = rFit.slice1(0, 1, 0 ,0, nDat, 1)
		number measuredShift = (rPos.GetPixel(0,0) - zoomCh)*epch
		number expectedShift = -HToffset
		number shiftError = (measuredShift - expectedShift)/expectedShift
		profile.SetNumberNote("Processing:ZPE Coefficient", shiftError*epch)
	}

	void mapDispersion(object self) {
		/*
			This function provides calls to fit data in the alignment
			measurements and then calculates a dispersion non-uniformity
			profile from the results.
		*/
		image dispProf
		self.fitZLP() // Execute first-pass single-pixel precision fit
		self.refineFitSubPix() // Execute sub-pixel fit refinement
		dispProf := self.generateProfile() // Execute nonuniformity calculation...
		// ... "self.generateProfile()" calculates the position dependent ...
		// ... variations in the dispersion and returns the resulting profile.
		self.calculateZPEnergyTerm(dispProf) // Determine the coefficient of the...
		// ... energy dependent term of the correction for later use.

		// Perform final modifications to profile
		dispProf /= epch // convert y values from energies into channel deviations
		// Check x-axis origin of dataset to accurately procure zoom channel
		if(zoomRef) {
			number og = dispProf.ImageGetDimensionOrigin(0)
			// "Normalise" the profile w.r.t the zoom channel of the spectrometer
			number offset = dispProf.GetPixel((zoomCh - og)/shiftStepCh, 0)
			dispProf -= offset
		}
		// Keep a permanent record of measured dispersion in metadata
		dispProf.SetNumberNote("Processing:Use Nominal Dispersion", useNomDisp)
		dispProf.SetNumberNote("Processing:nCh", nCh)
		dispProf.ImageSetDimensionUnitString(0,"Channel")
		dispProf.ShowImage() // Reveals profile to the user, 
		// can be saved for later use.
		
		interpol := dispProf // Assigns profile as class attribute to make it
		// visible to other functions; required for continuous execution of all steps
	}

	void polyFit(object self, number order) {
		/*
			Carry out a nth order polynomial fit to the dispersion profile.
			The results of the fit are then used to return a profile 
		*/
		if(!ImageIsValid(interpol)) { 
			if(!GetOneLabeledImageWithPrompt("Image Required", \
				"Please input nonlinearity profile.", "Profile", interpol) ) exit(0)
		}
		interpol.GetNumberNote("Processing:nCh", nCh)

		image data := interpol.ImageClone()
		image errors := data.ImageClone()*0 + 1
		number chiSq, conv
		chiSq = 1e6
		conv = 1e-8
		image pars := RealImage("Polynomial Coefficients", bD, order+1, 1)
		pars = -5
		image parsToFit := RealImage("tmp", bD, order+1, 1)
		parsToFit = 1

		number ok = FitPolynomial(data, errors, pars, parsToFit, chiSq, conv)
		// Generate dispersion map using polynomial
		image fit := RealImage("Order "+order+" Polynomial Non-Uniformity Profile", bD, nCh, 1)
		number og = data.ImageGetDimensionOrigin(0)
		for(number i=0; i<order+1; i++) { fit += ((icol - og)**i)*pars.GetPixel(i,0); }
		if(zoomRef) {
			number offset = fit.GetPixel(zoomCh, 0)
			fit -= offset
			data -= offset // also apply offset to profile that was fitted
		}
		// Carry forward measured dispersion and settings in metadata
		number nDisp, nom, chromcof
		data.GetNumberNote("Processing:Measured Dispersion", nDisp)
		data.GetNumberNote("Processing:Use Nominal Dispersion", nom)
		data.GetNumberNote("Processing:ZPE Coefficient", chromcof)
		fit.SetNumberNote("Processing:Use Nominal Dispersion", nom)
		fit.SetNumberNote("Processing:Measured Dispersion", nDisp)
		fit.SetNumberNote("Processing:ZPE Coefficient", chromcof)
		for(number i=0; i<order+1; i++) {
			fit.SetNumberNote("Processing:Fit Coefficients:c"+i, pars.GetPixel(i,0))
		}
		data.SetName("Non-Uniformity Profile to fit")
		fit.SetName("Order "+order+" Polynomial Non-Uniformity Profile")
		fit.ShowImage()
		// replace map class attribute with polynomial fit
		interpol := fit.ImageClone()
	}

	void chooseSpectra(object self, image low, image high, image map) {
		/*
			Provides a script interface to supply spectra and correction
			profiles for correctSpectra() without requiring user input via
			dialog boxes.
		*/
		LL := low.ImageClone()
		HL := high.ImageClone()
		interpol := map.ImageClone()
	}

	void transform(object self, image low, image high, image map) {
		/*
			For 1d and 2d datasets, perform interpolation in energy based on
			provided coordinate transformation map.
		*/
		number sx, sy, sz, dimsL, dimsH, nDisp, nom, ZPEcoef
		dimsL = ImageGetNumDimensions(low)
		dimsH = ImageGetNumDimensions(high)
		map.GetNumberNote("Processing:Measured Dispersion", nDisp)
		map.GetNumberNote("Processing:Use Nominal Dispersion", nom)
		map.GetNumberNote("Processing:ZPE Coefficient", ZPEcoef)
		// Get The Coefficients of the polynomial fit, used for calculating
		// ... the intensity correction factors
		taggroup tags, cTags
		tags = ImageGetTagGroup(map)
		TagGroupGetTagAsTagGroup(tags, "Processing:Fit Coefficients", cTags)
		number nCoefs = TagGroupCountTags(cTags)
		image coefsL = RealImage("Fit Coefficients", bD, nCoefs, 1)
		for(number i=0; i<nCoefs; i++) {
			number val
			cTags.TagGroupGetTagAsNumber("c"+i, val)
			coefsL[i,0] = val
		}
		coefsL[1,0] += 1 // add the 1 channel per channel term back in
		image coefsH := coefsL.ImageClone()

		if(dimsL != dimsH) 
			{ throw("DualEELS siblings have different dimensionality."); }
		image tempLow, tempHigh
		if(dimsL==3) {
		// 3D datasets have to be rotated prior to transformation via warp fn.
			low.Get3dSize(sx, sy, sz)
			tempLow := low.Slice3(sx-1, 0, 0, 2,sz, 1, 1,sy, 1, 0,sx,-1).ImageClone()
			tempHigh := high.Slice3(sx-1, 0, 0, 2,sz, 1, 1,sy, 1, 0,sx,-1).ImageClone()
		}
		else {
		// 1D and 2D data do not require preparatory treatment. 
			low.GetSize(sz, sy); sx = 1;
			tempLow := low.ImageClone()
			tempHigh := high.ImageClone()
		}
		// Calculate intensity correction factors based on derivative of 
		// transformation map.  See references for details.
		image IntCorrL = tempLow.slice2(0, 0, 0, 0, sz, 1, 1, sy, 1)*0
		image IntCorrH = tempLow.slice2(0, 0, 0, 0, sz, 1, 1, sy, 1)*0
		image mapL := map.ImageClone()
		image mapH := map.ImageClone()
		if(ZCor) { 
			// If using ZPE correction, the ZPE term is introduced...
			// ... at this point so that it is also factored into the ...
			//... intensity correction calculation
			number offsetL, offsetH
			offsetL = tempLow.ImageGetDimensionOrigin(0)
			offsetH = tempHigh.ImageGetDimensionOrigin(0)
			mapL += (icol + offsetL/epch) * ZPEcoef
			mapH += (icol + offsetH/epch) * ZPEcoef
			coefsL[1,0] += ZPEcoef
			coefsH[1,0] += ZPEcoef
			coefsL[0,0] += ZPEcoef * offsetL/epch
			coefsH[0,0] += ZPEcoef * offsetH/epch
		}
		// Analytically calculate intensity correction factors for the 
		// ... Jacobian transformation.
		for(number i=1; i<nCoefs; i++) {
			IntCorrL += i*coefsL.GetPixel(i,0)*icol**(i-1)
			IntCorrH += i*coefsH.GetPixel(i,0)*icol**(i-1)
		}
		// Apply pre-interpolation intensity correction
		image holdLL := tempLow.ImageClone()
		image holdHL := tempHigh.ImageClone()

		holdLL *= IntCorrL[icol, irow] // Multiplies data by intensity correction
		holdHL *= IntCorrH[icol, irow] // factor ahead of interpolation
		//Prepare an array of x-values to be fed to the interpolation
		image xmap = map.ImageClone()*0
		xmap = icol
		//Add the linear term icol to the non-uniformity maps, so that
		//they can be used to determine which points to sample in the 
		//interpolation
		mapL += icol
		mapH += icol
		//Initialise the interpolation routine
		object warp_LL = alloc(Poly_interp)
		object warp_HL = alloc(Poly_interp)
		warp_LL.init(xmap, holdLL, interp_param)
		warp_HL.init(xmap, holdHL, interp_param)
		// Loop through energy axis to carry out interpolation
		for(number i=0; i<sz; i++) {			
			// Interpolate slice values for current channel
			tempLow.slice2(i, 0, 0, 1, sy, 1, 2, sx, 1) = warp_LL.interp(mapL.GetPixel(i,0))
			tempHigh.slice2(i, 0, 0, 1, sy, 1, 2, sx, 1) = warp_HL.interp(mapH.GetPixel(i,0))
			if(remainder(Floor(100*(i+1/sz)),10)==0) {
				OpenAndSetProgressWindow("Interpolating across","energy dispersion", "Slice "+(i+1)+" of "+sz); }
		}
		// assign corrected data to relevant class attributes
		if(dimsL==3) {
			LL_CECI := tempLow.Slice3(0, 0, sx-1, 2, sx, -1, 1, sy, 1, 0, sz, 1).ImageClone()
			HL_CECI := tempHigh.Slice3(0, 0, sx-1, 2, sx, -1, 1, sy, 1, 0, sz, 1).ImageClone()
			if(!nom) {
				// If rebinning to measured dispersion, change calibration scale
				LL_CECI.ImageSetDimensionScale(2, nDisp)
				HL_CECI.ImageSetDimensionScale(2, nDisp)
			}
		}
		else {
			LL_CECI := tempLow.ImageClone()
			HL_CECI := tempHigh.ImageClone()
			if(!nom) {
				LL_CECI.ImageSetDimensionScale(0, nDisp)
				HL_CECI.ImageSetDimensionScale(0, nDisp)
			}
		}
		intCorrMapL := IntCorrL.ImageClone()
		intCorrMapH := IntCorrH.ImageClone()
	}

	void updateMetaData(object self, image low, image high, image map, image intMapL, image intMapH) {
		/*
			Update the metadata of corrected spectra to include information
			about the processing, as well as updating the UID tags so that
			DigitalMicrograph will recognise the dualEELS pair as siblings.
		*/
		// First, retrieve unique IDs of images to identifty siblings in tags
		object lowUID = ImageGetUniqueID(low)
		object highUID = ImageGetUniqueID(high)

		number L1, L2, L3, L4, H1, H2, H3, H4
		lowUID.GetValues(L1, L2, L3, L4)
		highUID.GetValues(H1, H2, H3, H4)

		taggroup lowTags = ImageGetTagGroup(low)
		taggroup highTags = ImageGetTagGroup(high)
		// Assign UID values of sibling to tags of each image to retain sibling relations
		lowTags.TagGroupSetTagAsLong("EELS:Acquisition:Dual acquire sibling:UID:0", H1)
		lowTags.TagGroupSetTagAsLong("EELS:Acquisition:Dual acquire sibling:UID:1", H2)
		lowTags.TagGroupSetTagAsLong("EELS:Acquisition:Dual acquire sibling:UID:2", H3)
		lowTags.TagGroupSetTagAsLong("EELS:Acquisition:Dual acquire sibling:UID:3", H4)

		highTags.TagGroupSetTagAsLong("EELS:Acquisition:Dual acquire sibling:UID:0", L1)
		highTags.TagGroupSetTagAsLong("EELS:Acquisition:Dual acquire sibling:UID:1", L2)
		highTags.TagGroupSetTagAsLong("EELS:Acquisition:Dual acquire sibling:UID:2", L3)
		highTags.TagGroupSetTagAsLong("EELS:Acquisition:Dual acquire sibling:UID:3", L4)

		// Set dispersion correction profile image as array tag
		lowTags.TagGroupSetTagAsArray("EELS:Processing:Dispersion Correction Map", map)
		highTags.TagGroupSetTagAsArray("EELS:Processing:Dispersion Correction Map", map)
		// Set dispersion correction profile image as array tag
		lowTags.TagGroupSetTagAsArray("EELS:Processing:Intensity Correction Map", intMapL)
		highTags.TagGroupSetTagAsArray("EELS:Processing:Intensity Correction Map", intMapH)
		//Copy chromatic error tag over and whether chromatic term was used or not
		number ZPEcoef
		map.GetNumberNote("Processing:ZPE Coefficient", ZPEcoef)
		low.SetNumberNote("EELS:Processing:ZPE Coefficient", ZPEcoef)
		high.SetNumberNote("EELS:Processing:ZPE Coefficient", ZPEcoef)
		low.SetNumberNote("EELS:Processing:ZPE Correction?", ZCor)
		high.SetNumberNote("EELS:Processing:ZPE Correction?", ZCor)
	}

	void saveResults(object self, image low, image high) {
		/*
			Save spectra with path and filename specified by the user.
		*/
		string LLname, HLname
		GetName(low, LLname)
		GetName(high, HLname)
		SaveAsDialog("Choose where to save low-loss spectra.", LLname, LLname)
		low.SaveAsGatan(LLname)
		SaveAsDialog("Choose where to save high-loss spectra.", HLname, HLname)
		high.SaveAsGatan(HLname)

		OKDialog("Spectra saved.")
	}

	void correctSpectra(object self) {
		/*
			This function provides calls to apply corrections to real spectra
			or spectrum images based on a calculated nonuniformity profile

		*/
		if( !ImageIsValid(interpol) || !ImageIsValid(LL) ) { 
			if(!GetThreeLabeledImagesWithPrompt( "Images Required", \
				"Please provide data to be processed \n and interpolation profile.", \
				"Low-loss:", LL, "High-loss", HL, "Interpolant", interpol) ) exit(0); }

		String Name_LL, Name_HL
		Name_LL=GetName(LL)
		Name_HL=GetName(HL)

		self.transform(LL, HL, interpol)

		self.updateMetaData(LL_CECI, HL_CECI, interpol, intCorrMapL, intCorrMapH)

		SetName(LL_CECI, Name_LL+" (CorrE)(CorrI)"); LL_CECI.ShowImage();
		SetName(HL_CECI, Name_HL+" (CorrE)(CorrI)"); HL_CECI.ShowImage();

		if(!OKCancelDialog("Save corrected spectra/SIs?")) exit(0)
		else self.saveResults(LL_CECI, HL_CECI)
	}
}

///////////////////////////////////////////////////////////////////////////////
//		INITIAL USER INTERFACE DIALOG BOX
///////////////////////////////////////////////////////////////////////////////

Class stepDialog : UIFrame {
	/*
		This class generates a small custom dialog which allows the user to 
		choose a single processing step from within the script.
	*/
	taggroup RadioList
	// Function which allows chosen value to be read:
	Number ReturnRadioList(object self) return RadioList.DLGGetValue() 

	Object Init(object self) {
		taggroup DLGitems = DLGCreateDialog("Choose Option")
		RadioList = DLGCreateRadioList()
		RadioList.DLGAddRadioItem("Run All Processing Steps", 1)
		RadioList.DLGAddRadioItem("Map Dispersion Non-uniformities", 2)
		RadioList.DLGAddRadioItem("Fit Nonlinearity Profile", 3)
		RadioList.DLGAddRadioItem("Linearise Spectra", 4)
		DLGitems.DLGAddElement(RadioList)
		return self.super.Init(DLGitems)
	}
}

///////////////////////////////////////////////////////////////////////////////
//		SCRIPT EXECUTION
///////////////////////////////////////////////////////////////////////////////

void begin() {
	// Allocate correction methods to object instance
	Object methods = Alloc(correctionProcs)

	number step
	// Create a small GUI dialog interface which allows user to choose what
	// methods to implement
	object diveIn = Alloc(stepDialog).init()
	// Ask the user whether they want to run all steps, or a specific step
	diveIn.pose()
	// Check what the response is:
	step = diveIn.ReturnRadioList()
	// Establish what methods are executed for each condition
	if(step==1) { // User wants to run all processing steps
		methods.init(1)
		methods.mapDispersion()
		if(usePolyFit) { methods.polyFit(polyOrd); }
		methods.correctSpectra()
	}
	// User wants to process up to generating dispersion profile
	if(step==2) {
		methods.init(0)
		methods.mapDispersion()
	}
	// User wants to fit polynomial to disperison profile
	if(step==3) { methods.polyFit(polyOrd); } 
	// User wants to use profile to correct dualEELS spectra.
	if(step==4) { methods.correctSpectra(); }
	// Note that if the user presses cancel on the UI, the dialog returns 0
	// and the script ends.
}

begin()